************************************************************************************************************************************************
U folderu Kodovi nalaze se Java i R kodovi izrađeni u sklopu projektnog zadatka.

Java kodovi neposredno prate sve dijelove projekta opisane u radu.
R kodovi su kodovi koji su se koristili za prikaz podataka i greške.

================================================================================================================================================
UPUTE ZA POKRETANJE (za Linux sustave):
================================================================================================================================================
Za pokretanje aplikacije potrebna je Java verzije 8.

U folderu ChordRecognition pokrenite sljedeću naredbu:

java -cp bin:lib/* machine.learning.project.chords.test.TestingModel 1 svm.model test.data 2 predictions.csv error.csv

1 - označava da se koristi SVM algoritam
svm.model - ime datoteke u kojoj je pohranjen istreniran Weka model (datoteka se nalazi u folderu ChordRecognition)
test.data - ime datoteke u CSV formatu koja sadrži skup podataka za testiranje (datoteka se nalazi u folderu ChordRecognition)
2 - označava da se koriste svi atributi (Faza 3 u radu)
predictions.csv - ime datoteke u koju će biti pohranjene predikcije 
error.csv - ime datoteke u koju će biti pohranjen vektor koji opisuje vrstu greške (kao što je opisano u radu)

Ova konfiguracija pokreće evaluaciju završnog modela opisanog u radu, na kojem je postignuta najbolja točnost na skupu za testiranje. 
************************************************************************************************************************************************
Opis programa (javadoc):

Program for testing and evaluating trained models that are used for solving chord recognition problem. It takes 6 command line arguments. First one is a model code, an integer specifying which algorithm is used for modeling. Second one is a Weka model file name which contains a model to be used for classification, third one is the name of a file in CSV format containing test set, the fourth one is an attribute code (described in the paper), the fifth one is the name of the file in which predictions of the classification algorithm will be stored and, finally, the sixth one is the name of a file in which error type information of the classification algorithm will be stored (as described in the paper). Following model codes are valid:
0 - RandomForest
1 - LibSVM
2 - J48
3 - Stacking
Following attribute codes are valid:
0 - only sequential attributes
1 - only standard, non sequential attributes (Phase 2 in paper)
2 - all attributes (Phase 3 in paper)
3 - only raw features (Phase 1 in paper)

************************************************************************************************************************************************
U folderu ChordRecognition nalazi se folder doc u kojem je smješten ostatak javadoc dokumentacije.
************************************************************************************************************************************************
Ana Paliska
Filip Srnec
